# ESGI

# Expertise mission
Consultant in a Digital Services Company (ESN)

## Group members
Group 4:
- ALAA HAZIM Zaid
- ACHBOUQ Slimane
- Zakaria ATTAOUI

## Goal
implement good practices on a project.

## Required work
set up best practices on a project recently acquired by the client company, and thus encourage the addition of new functionalities by a team that will be put together after your visit.

## What to use?
It is recommended :
- to use PHP in version 5.6
- And MySQL;


The work to be returned must include all the files necessary to get started with the project,
basically the source code and the database script.

The work is to be done using Gitlab repository, Gitlab Boards, Gitlab Issues, Gitlab CI, Github repository and Heroku in push to deploy.

## Link to GitHub

https://github.com/slimane-achbouq/Api-migration
